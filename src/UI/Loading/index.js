import React, { Fragment } from "react";
import { Paragraph, Loader } from "rsuite";

const Loading = () => {
  return (
    <Paragraph rows={8}>
      <Loader backdrop center content="loading..." vertical />
    </Paragraph>
  );
};

export default Loading;
