import React from "react";
import Avatar from "react-avatar";

const AvatarProfile = props => <Avatar {...props} />;

export default AvatarProfile;
