import React from "react";
import { Chat, Channel, ChannelHeader, Window } from "stream-chat-react";
import {
  MessageList,
  MessageInput,
  MessageLivestream
} from "stream-chat-react";
import { MessageInputSmall, Thread } from "stream-chat-react";
import { StreamChat } from "stream-chat";

import "stream-chat-react/dist/css/index.css";

import { Drawer } from "rsuite";

const chatClient = new StreamChat("4sdvgm489vsb");

chatClient.updateAppSettings({
  disable_auth_checks: true
});

chatClient.setUser(
  {
    id: "jlahey",
    name: "Danilo Vaz",
    image: "/images/danilo.png"
  },
  "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoiamxhaGV5In0.0RUxlj9CY0Jww_SZTTbyIBAHFKOe3Ipc8TsUGrO2_mw"
);

// chatClient.setUser(
//   {
//     id: "john",
//     name: "John Doe",
//     image: "https://getstream.io/random_svg/?name=John"
//   },
//   "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoiam9obiJ9.S40gl-Mi9rGrQ4XF1pPi5ECYPEiLOHZYB3DfNDev9o8"
// );

const channel = chatClient.channel("livestream", "QueroFormar", {
  image: "/Logo-vertical.svg",
  name: "Chat - Quero Formar"
});

const ConversationThread = () => (
  <Chat client={chatClient} theme={"livestream dark"}>
    <Channel channel={channel} Message={MessageLivestream}>
      <Window hideOnThread>
        <ChannelHeader live />
        <MessageList />
        <MessageInput Input={MessageInputSmall} focus />
      </Window>
      <Thread fullWidth />
    </Channel>
  </Chat>
);

export default ConversationThread;

{
  /* <DatePicker
  format="YYYY-MM-DD HH:mm:ss"
  ranges={[
    {
      label: "Now",
      value: new Date()
    }
  ]}
/>; */
}
