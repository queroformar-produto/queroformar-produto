import React from "react";
import { Tag } from "rsuite";

const TagElement = props => <Tag {...props}> {props.children} </Tag>;

export default TagElement;
