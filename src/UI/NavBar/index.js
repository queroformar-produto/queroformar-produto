import React, { Fragment } from "react";
import { Navbar, Nav, Dropdown, Icon } from "rsuite";
import { Link } from 'react-router-dom';
import "./index.css";

const NavBar = () => {

  return (
    <Fragment>
      <Navbar className="navbar">
        <Navbar.Header>
          <Link to="/home"><img className="logo" src="/Logo.svg" alt="Logo Quero Formar"/></Link>
        </Navbar.Header>
        <Navbar.Body>
          <Nav>
            <Nav.Item icon={<Icon icon="home" />}><Link to="/home">Início</Link></Nav.Item>
            <Nav.Item>Aluno</Nav.Item>
            <Nav.Item><Link to="/search/mentor">Mentor</Link></Nav.Item>
          </Nav>
          <Nav pullRight>
            <Nav.Item icon={<Icon icon="cog" />}>Configurações</Nav.Item>
          </Nav>
        </Navbar.Body>
      </Navbar>
    </Fragment>
  );
};

export default NavBar;
