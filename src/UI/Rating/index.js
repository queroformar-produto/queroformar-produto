import React from "react";
import Rater from "react-rater";
import "react-rater/lib/react-rater.css";
import "./index.css";

const Rating = props => <Rater {...props} />;

export default Rating;
