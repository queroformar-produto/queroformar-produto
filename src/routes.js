import React from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { withRouter } from "react-router-dom";
import { userIsAuthenticatedRedir, userIsNotAuthenticatedRedir } from "./auth";

import { LoginContainer } from "./modules/authentication/Login/components";
import { ProtectedRouteContainer } from "./modules/authentication/ProtectedRoute/components";
import { SignupContainer } from "./modules/authentication/Signup/components";
import { HomeContainer } from "./modules/home/Home/components";
import { MentorListContainer } from "./modules/mentor/MentorList/components";
import { ConversationWithMentorContainer } from "./modules/conversation/ConversationWithMentor/components";

// const Login = withRouter(userIsNotAuthenticatedRedir(LoginContainer));
// const Protected = withRouter(userIsAuthenticatedRedir(ProtectedRouteContainer));

const Login = withRouter(LoginContainer);
const Protected = withRouter(ProtectedRouteContainer);

const Routes = () => (
  <Router>
    <Switch>
      <Route exact path="/login" component={Login} />
      <Route exact path="/signup" component={SignupContainer} />

      <Protected>
        <Route exact path="/" component={HomeContainer} />
        <Route exact path="/search/mentor" component={MentorListContainer} />
        <Route
          exact
          path="/conversation"
          component={ConversationWithMentorContainer}
        />

        {/* <Route
          exact
          path="/publication/:uuid"
          component={PublicationWrapperContainer}
        /> */}
      </Protected>
    </Switch>
  </Router>
);

export default compose(connect(state => ({ user: state.user }))(Routes));
