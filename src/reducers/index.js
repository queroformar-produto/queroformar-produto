import { combineReducers } from "redux";
import storage from "redux-persist/lib/storage";
import { persistReducer } from "redux-persist";
import autoMergeLevel2 from "redux-persist/lib/stateReconciler/autoMergeLevel2";
import hardSet from "redux-persist/lib/stateReconciler/hardSet";

import { user } from "../modules/authentication/Login/__redux__/reducer";
import {
  course,
  selectedCourseByUniversity
} from "../modules/course/CourseListByUniversity/__redux__/reducer";
import {
  university,
  selectedUniversity
} from "../modules/university/UniversityList/__redux__/reducer";
import {
  tag,
  mentor
} from "../modules/mentor/ProfileSummary/__redux__/reducer";

const userPersistConfig = {
  key: "user",
  storage: storage,
  stateReconciler: autoMergeLevel2
};

const universityPersistConfig = {
  key: "university",
  storage: storage
};

const selectedUniversityPersistConfig = {
  key: "selectedUniversity",
  storage: storage
};

const coursePersistConfig = {
  key: "course",
  storage: storage
};

const selectedCourseByUniversityPersistConfig = {
  key: "selectedCourseByUniversity",
  storage: storage
};

const tagPersistConfig = {
  key: "tag",
  storage: storage
};

const mentorPersistConfig = {
  key: "mentor",
  storage: storage
};

const rootReducer = combineReducers({
  user: persistReducer(userPersistConfig, user),
  university: persistReducer(universityPersistConfig, university),
  selectedUniversity: persistReducer(
    selectedUniversityPersistConfig,
    selectedUniversity
  ),
  course: persistReducer(coursePersistConfig, course),
  selectedCourseByUniversity: persistReducer(
    selectedCourseByUniversityPersistConfig,
    selectedCourseByUniversity
  ),
  tag: persistReducer(tagPersistConfig, tag),
  mentor: persistReducer(mentorPersistConfig, mentor)
});

export default rootReducer;
