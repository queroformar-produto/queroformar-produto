import { compose } from "redux";
import { connect } from "react-redux";
import {
  getUniversityList,
  selectUniversity
} from "../__redux__/actions";

import University from "./UniversityList";

export default compose(
  connect(
    (state, ownProps) => ({
      university: state.university,
      selectedUniversity: state.selectUniversity,
      currentUrl: ownProps.history
    }),
    {
      getUniversityList,
      selectUniversity
    },
    (stateProps, dispatchProps, parentProps) => ({
      ...stateProps,
      ...parentProps,
      getUniversityList: () => dispatchProps.getUniversityList(),
      selectUniversity: (selected) => dispatchProps.selectUniversity(selected)
    })
  )(University)
);
