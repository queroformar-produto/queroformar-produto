import React, { Component } from "react";
import { Button, Drawer, SelectPicker } from "rsuite";

export class UniversityList extends Component {
  constructor(props) {
    super(props);

    this.selectUniversity = this.selectUniversity.bind(this);
  }

  componentDidMount() {
    const {getUniversityList} =  this.props;

    getUniversityList()
  }

  selectUniversity(value, event) {
    const {selectUniversity} = this.props;

    selectUniversity(value);
  }


  render() {
    const { university } = this.props;

    return (
      <SelectPicker data={university.data} block onChange={(value, event) => this.selectUniversity(value, event)}/>
    );
  }
}

export default UniversityList;
