import { REQUEST_UNIVERSITY_LIST, RECEIVE_UNIVERSITY_LIST, SELECT_UNIVERSITY } from "./actions";

const initialState = {
  isLoading: false
};

export const university = (state = initialState, action) => {
  switch (action.type) {
    case REQUEST_UNIVERSITY_LIST:
      return {
        ...state,
        isLoading: true
      };
    case RECEIVE_UNIVERSITY_LIST:
      return {
        ...state,
        data: action.payload.map((item) => ({
          value: item.cnpj,
          label: item.razaosocial
        })),
        isLoading: false
      };
    default:
      return state;
  }
};

export const selectedUniversity = (state = initialState, action) => {
  switch (action.type) {
    case SELECT_UNIVERSITY:
      return {
        data: { cnpj: action.payload }
      };
    default:
      return state;
  }
};
