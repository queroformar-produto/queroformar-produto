import { HOST_API } from "../../../../constants/hosts";

import axios from "axios";

import {getCourseList} from "../../../course/CourseListByUniversity/__redux__/actions";

export const REQUEST_UNIVERSITY_LIST = "REQUEST_UNIVERSITY_LIST";
export const RECEIVE_UNIVERSITY_LIST = "RECEIVE_UNIVERSITY_LIST";
export const SELECT_UNIVERSITY = "SELECT_UNIVERSITY";

export const requestUniversityList = () => ({
  type: REQUEST_UNIVERSITY_LIST
});

export const receiveUniversityList = data => ({
  type: RECEIVE_UNIVERSITY_LIST,
  payload: data
});

export const selectingUniversity = (selected) => ({
  type: SELECT_UNIVERSITY,
  payload: selected
});

export const getUniversityList = () => (dispatch, getState) => {
    dispatch(requestUniversityList());

    axios
      .get(`${HOST_API}/universidade/all`)
      .then(function(response) {
        dispatch(receiveUniversityList(response.data));
      })
      .catch(function(error) {});
};

export const selectUniversity = (selectedUniversity) => dispatch => {
  dispatch(selectingUniversity(selectedUniversity));

  dispatch(getCourseList(selectedUniversity));


};
