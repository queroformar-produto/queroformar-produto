import React, { Component } from "react";
import { Button, Drawer, SelectPicker } from "rsuite";

export class CourseList extends Component {
  constructor(props) {
    super(props);

    this.selectCourse = this.selectCourse.bind(this);
  }

  selectCourse(value, event) {
    const {selectCourse} = this.props;

    selectCourse(value);
  }


  render() {
    const { selectedUniversity, course } = this.props;

    return (
      <SelectPicker data={course.data} block onChange={(value, event) => this.selectCourse(value, event)}/>
    );
  }
}

export default CourseList;
