import { compose } from "redux";
import { connect } from "react-redux";
import {
  getCourseList,
  selectCourse
} from "../__redux__/actions";

import Course from "./CourseList";

export default compose(
  connect(
    (state, ownProps) => ({
      selectedUniversity: state.selectedUniversity,
      course: state.course,
      selectedCourse: state.selectCourse,
      currentUrl: ownProps.history
    }),
    {
      getCourseList,
      selectCourse
    },
    (stateProps, dispatchProps, parentProps) => ({
      ...stateProps,
      ...parentProps,
      getCourseList: (selectedUniversity) => dispatchProps.getCourseList(selectedUniversity),
      selectCourse: (selected) => dispatchProps.selectCourse(selected)
    })
  )(Course)
);
