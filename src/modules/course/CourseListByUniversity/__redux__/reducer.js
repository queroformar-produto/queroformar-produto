import { REQUEST_COURSE_LIST, RECEIVE_COURSE_LIST, SELECT_COURSE } from "./actions";

const initialState = {
  isLoading: false
};

export const course = (state = initialState, action) => {
  switch (action.type) {
    case REQUEST_COURSE_LIST:
      return {
        ...state,
        isLoading: true
      };
    case RECEIVE_COURSE_LIST:
      return {
        ...state,
        data: action.payload.map((item) => ({
          label: item.curso,
          value: item.curso
        })),
        isLoading: false
      };
    default:
      return state;
  }
};

export const selectedCourseByUniversity = (state = initialState, action) => {
  switch (action.type) {
    case SELECT_COURSE:
      return {
        data: action.payload,
      };
    default:
      return state;
  }
};
