import { HOST_API } from "../../../../constants/hosts";

import axios from "axios";

export const REQUEST_COURSE_LIST = "REQUEST_COURSE_LIST";
export const RECEIVE_COURSE_LIST = "RECEIVE_COURSE_LIST";
export const SELECT_COURSE = "SELECT_COURSE";

export const requestCourseList = () => ({
  type: REQUEST_COURSE_LIST
});

export const receiveCourseList = data => ({
  type: RECEIVE_COURSE_LIST,
  payload: data
});

export const selectingCourse = (selected) => ({
  type: SELECT_COURSE,
  payload: selected
});

export const getCourseList = (selectedUniversity) => (dispatch, getState) => {
    dispatch(requestCourseList());

    axios
      .get(`${HOST_API}/curso/cursoUniversidade?cnpj=${selectedUniversity}`)
      .then(function(response) {
        dispatch(receiveCourseList(response.data));
      })
      .catch(function(error) {});
};

export const selectCourse = (selectedCourse) => dispatch => {
  dispatch(selectingCourse(selectedCourse));

};
