import { HOST_API } from "../../../../constants/hosts";

import axios from "axios";

export const REQUEST_TAGS = "REQUEST_TAGS";
export const RECEIVE_TAGS = "RECEIVE_TAGS";
export const REQUEST_MENTORS = "REQUEST_MENTORS";
export const RECEIVE_MENTORS = "RECEIVE_MENTORS";
export const SELECT_TAG = "SELECT_TAG";

export const requestTags = () => ({
  type: REQUEST_TAGS
});

export const receiveTags = data => ({
  type: RECEIVE_TAGS,
  payload: data
});

export const requestMentors = () => ({
  type: REQUEST_MENTORS
});

export const receiveMentors = data => ({
  type: RECEIVE_MENTORS,
  payload: JSON.parse(data)
});

export const selectingTag = selected => ({
  type: SELECT_TAG,
  payload: selected
});

export const getTags = () => dispatch => {
  dispatch(requestTags());

  axios
    .get(`${HOST_API}/tag/all`)
    .then(function(response) {
      dispatch(receiveTags(response.data));
    })
    .catch(function(error) {});
};

export const getMentors = () => dispatch => {
  dispatch(requestMentors());

  axios
    .get(`${HOST_API}/mentor/getMentores`)
    .then(function(response) {
      dispatch(receiveMentors(response.data));
    })
    .catch(function(error) {});
};

export const selectTag = selected => dispatch => {
  dispatch(selectingTag(selected));
};
