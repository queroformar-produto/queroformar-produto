import {
  REQUEST_TAGS,
  RECEIVE_TAGS,
  SELECT_TAG,
  REQUEST_MENTORS,
  RECEIVE_MENTORS
} from "./actions";

const initialState = {
  isLoading: false
};

export const mentor = (state = initialState, action) => {
  switch (action.type) {
    case REQUEST_MENTORS:
      return {
        ...state,
        isLoading: true
      };
    case RECEIVE_MENTORS:
      return {
        ...state,
        data: action.payload,
        isLoading: false
      };
    default:
      return state;
  }
};

//{"cpf":"cpf1","nome":"Ian Mckellen","idCidade":"3549904","cep":"12234532","email":"ian@gandalf.com","senha":"123","pic":"https://cdn.pensador.com/img/authors/ga/nd/gandalf-l.jpg","bio":"Mestre das Aventuras, Guia e mentor de homens pequenos","universidade":[],"cursos":[],"funcoes":[{"id":1,"funcao":"Mentor"}],"skills":[{"idTag":104,"tag":"C#"},{"idTag":108,"tag":"Python"},{"idTag":116,"tag":"POO"},{"idTag":139,"tag":"Algoritmo"},{"idTag":141,"tag":"Logica de Programacao"},{"idTag":142,"tag":"Programacao"}],"nota":5.0}

export const tag = (state = initialState, action) => {
  switch (action.type) {
    case REQUEST_TAGS:
      return {
        ...state,
        isLoading: true
      };
    case RECEIVE_TAGS:
      return {
        ...state,
        data: action.payload,
        isLoading: false
      };
    default:
      return state;
  }
};

export const selectedTag = (state = initialState, action) => {
  switch (action.type) {
    case SELECT_TAG:
      return {
        data: action.payload
      };
    default:
      return state;
  }
};
