import { compose } from "redux";
import { connect } from "react-redux";
import { getTags, selectTag, getMentors } from "../__redux__/actions";

import ProfileSummary from "./ProfileSummary";

export default compose(
  connect(
    (state, ownProps) => ({
      tag: state.tag,
      mentors: state.mentor,
      currentUrl: ownProps.history
    }),
    {
      getTags,
      selectTag,
      getMentors
    },
    (stateProps, dispatchProps, parentProps) => ({
      ...stateProps,
      ...parentProps,
      getMentors: () => dispatchProps.getMentors(),
      getTags: () => dispatchProps.getTags(),
      selectTag: selected => dispatchProps.selectTag(selected)
    })
  )(ProfileSummary)
);
