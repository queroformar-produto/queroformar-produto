import React, { Component, Fragment } from "react";
import Rating from "../../../../UI/Rating";
import AvatarProfile from "../../../../UI/AvatarProfile";
import TagElement from "../../../../UI/TagElement";

import {
  ButtonToolbar,
  Button,
  FlexboxGrid,
  TagGroup,
  Row,
  Col,
  Panel
} from "rsuite";

import "./index.css";

export class ProfileSummary extends Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    const { getTags, getMentors } = this.props;

    getTags();

    getMentors();
  }

  goToChat = e => {
    this.props.history.push("/schedule");
  };

  render() {
    const { tag, mentors } = this.props;

    return (
      <Fragment>
        <Panel bordered className="panel-padding">
          <Row className="verticalCenter">
            <Col md={5}>
              <FlexboxGrid justify="center">
                <FlexboxGrid.Item colspan={22} style={{ textAlign: "center" }}>
                  <AvatarProfile
                    name="Danilo Vaz"
                    size="150"
                    round={true}
                    src={"/images/danilo.png"}
                  />
                </FlexboxGrid.Item>
                <FlexboxGrid.Item
                  colspan={22}
                  style={{ textAlign: "center", marginTop: "1em" }}
                >
                  <Rating total={5} rating={4} interactive={true} />
                </FlexboxGrid.Item>
                <FlexboxGrid.Item
                  colspan={22}
                  style={{ textAlign: "center", marginTop: "0.4em" }}
                >
                  <span
                    style={{
                      fontSize: "12px"
                    }}
                  >
                    27 Avaliações
                  </span>
                </FlexboxGrid.Item>
              </FlexboxGrid>
            </Col>
            <Col md={13}>
              <div className="summary">
                <h2>Danilo Vaz</h2>
                <div className="degree">
                  <p>
                    <strong>Graduação:</strong> Análise e Desenvolvimento de
                    Sistemas <strong>(Faculdade Impacta)</strong>
                  </p>
                  <strong>São Paulo / SP </strong>
                </div>
              </div>

              <TagGroup className="tagGroup">
                {tag &&
                  tag.data &&
                  tag.data.map(item => (
                    <TagElement key={item.idTag} color="violet">
                      <span>{item.tag}</span>
                    </TagElement>
                  ))}
              </TagGroup>

              <div className="about">
                <p>
                  9 anos de experiência com desenvolvimento web, 7 como
                  especialista Front-End. Palestrante, professor entusiasta open
                  source.
                </p>
              </div>
            </Col>
            <Col md={6}>
              <FlexboxGrid justify="space-around" align="middle">
                <FlexboxGrid.Item colspan={22}>
                  <ButtonToolbar>
                    <Button
                      appearance="primary"
                      block
                      onClick={e => this.goToChat(e)}
                    >
                      Agendar horário
                    </Button>
                  </ButtonToolbar>
                </FlexboxGrid.Item>
                <FlexboxGrid.Item colspan={22}>
                  <p className="mentorAvailable">
                    <strong>Disponível:</strong> {"Hoje"}
                  </p>
                </FlexboxGrid.Item>
              </FlexboxGrid>
            </Col>
          </Row>
        </Panel>
        <Panel bordered className="panel-padding">
          <Row className="verticalCenter">
            <Col md={5}>
              <FlexboxGrid justify="center">
                <FlexboxGrid.Item colspan={22} style={{ textAlign: "center" }}>
                  <AvatarProfile name="Safire Lauene" size="150" round={true} />
                </FlexboxGrid.Item>
                <FlexboxGrid.Item
                  colspan={22}
                  style={{ textAlign: "center", marginTop: "1em" }}
                >
                  <Rating total={5} rating={4} interactive={true} />
                </FlexboxGrid.Item>
                <FlexboxGrid.Item
                  colspan={22}
                  style={{ textAlign: "center", marginTop: "0.4em" }}
                >
                  <span
                    style={{
                      fontSize: "12px"
                    }}
                  >
                    21 Avaliações
                  </span>
                </FlexboxGrid.Item>
              </FlexboxGrid>
            </Col>
            <Col md={13}>
              <div className="summary">
                <h2>Safire Lauene</h2>
                <div className="degree">
                  <p>
                    <strong>Especialização:</strong> CELTA(Cambridge Certificate
                    in Teaching English) <strong>(Cambridge University)</strong>
                  </p>
                  <strong>São Paulo / SP </strong>
                </div>
              </div>

              <TagGroup className="tagGroup">
                {tag &&
                  tag.data &&
                  tag.data.map(item => (
                    <TagElement key={item.idTag} color="violet">
                      <span>{item.tag}</span>
                    </TagElement>
                  ))}
              </TagGroup>

              <div className="about">
                <p>
                  Professor 5 estrelas no Profes.15 anos de experiência.
                  Certificado CELTA Cambridge. Aulas personalizadas.Todos os
                  níveis. TOEFL,IELTS,TOEIC,SAT.
                </p>
              </div>
            </Col>
            <Col md={6}>
              <FlexboxGrid justify="space-around" align="middle">
                <FlexboxGrid.Item colspan={22}>
                  <ButtonToolbar>
                    <Button
                      appearance="primary"
                      block
                      onClick={e => this.goToChat(e)}
                    >
                      Agendar horário
                    </Button>
                  </ButtonToolbar>
                </FlexboxGrid.Item>
                <FlexboxGrid.Item colspan={22}>
                  <p className="mentorAvailable">
                    <strong>Disponível:</strong> {"Próximos dias"}
                  </p>
                </FlexboxGrid.Item>
              </FlexboxGrid>
            </Col>
          </Row>
        </Panel>
        <Panel bordered className="panel-padding">
          <Row className="verticalCenter">
            <Col md={5}>
              <FlexboxGrid justify="center">
                <FlexboxGrid.Item colspan={22} style={{ textAlign: "center" }}>
                  <AvatarProfile name="Rebeca Prado" size="150" round={true} />
                </FlexboxGrid.Item>
                <FlexboxGrid.Item
                  colspan={22}
                  style={{ textAlign: "center", marginTop: "1em" }}
                >
                  <Rating total={5} rating={4} interactive={true} />
                </FlexboxGrid.Item>
                <FlexboxGrid.Item
                  colspan={22}
                  style={{ textAlign: "center", marginTop: "0.4em" }}
                >
                  <span
                    style={{
                      fontSize: "12px"
                    }}
                  >
                    20 Avaliações
                  </span>
                </FlexboxGrid.Item>
              </FlexboxGrid>
            </Col>
            <Col md={13}>
              <div className="summary">
                <h2>Rebeca Prado</h2>
                <div className="degree">
                  <p>
                    <strong>Especialização:</strong> CELTA(Cambridge Certificate
                    in Teaching English) <strong>(Cambridge University)</strong>
                  </p>
                  <strong>São Paulo / SP </strong>
                </div>
              </div>

              <TagGroup className="tagGroup">
                {tag &&
                  tag.data &&
                  tag.data.map(item => (
                    <TagElement key={item.idTag} color="violet">
                      <span>{item.tag}</span>
                    </TagElement>
                  ))}
              </TagGroup>

              <div className="about">
                <p>
                  Professor 5 estrelas no Profes.15 anos de experiência.
                  Certificado CELTA Cambridge. Aulas personalizadas.Todos os
                  níveis. TOEFL,IELTS,TOEIC,SAT.
                </p>
              </div>
            </Col>
            <Col md={6}>
              <FlexboxGrid justify="space-around" align="middle">
                <FlexboxGrid.Item colspan={22}>
                  <ButtonToolbar>
                    <Button
                      appearance="primary"
                      block
                      onClick={e => this.goToChat(e)}
                    >
                      Agendar horário
                    </Button>
                  </ButtonToolbar>
                </FlexboxGrid.Item>
                <FlexboxGrid.Item colspan={22}>
                  <p className="mentorAvailable">
                    <strong>Disponível:</strong> {"Semana que vem"}
                  </p>
                </FlexboxGrid.Item>
              </FlexboxGrid>
            </Col>
          </Row>
        </Panel>
        <Panel bordered className="panel-padding">
          <Row className="verticalCenter">
            <Col md={5}>
              <FlexboxGrid justify="center">
                <FlexboxGrid.Item colspan={22} style={{ textAlign: "center" }}>
                  <AvatarProfile
                    name="Rodrigo Takeshi"
                    size="150"
                    round={true}
                  />
                </FlexboxGrid.Item>
                <FlexboxGrid.Item
                  colspan={22}
                  style={{ textAlign: "center", marginTop: "1em" }}
                >
                  <Rating total={5} rating={3} interactive={true} />
                </FlexboxGrid.Item>
                <FlexboxGrid.Item
                  colspan={22}
                  style={{ textAlign: "center", marginTop: "0.4em" }}
                >
                  <span
                    style={{
                      fontSize: "12px"
                    }}
                  >
                    20 Avaliações
                  </span>
                </FlexboxGrid.Item>
              </FlexboxGrid>
            </Col>
            <Col md={13}>
              <div className="summary">
                <h2>Rodrigo Takeshi</h2>
                <div className="degree">
                  <p>
                    <strong>Especialização:</strong> CELTA(Cambridge Certificate
                    in Teaching English) <strong>(Cambridge University)</strong>
                  </p>
                  <strong>São Paulo / SP </strong>
                </div>
              </div>

              <TagGroup className="tagGroup">
                {tag &&
                  tag.data &&
                  tag.data.map(item => (
                    <TagElement key={item.idTag} color="violet">
                      <span>{item.tag}</span>
                    </TagElement>
                  ))}
              </TagGroup>

              <div className="about">
                <p>
                  Professor 5 estrelas no Profes.15 anos de experiência.
                  Certificado CELTA Cambridge. Aulas personalizadas.Todos os
                  níveis. TOEFL,IELTS,TOEIC,SAT.
                </p>
              </div>
            </Col>
            <Col md={6}>
              <FlexboxGrid justify="space-around" align="middle">
                <FlexboxGrid.Item colspan={22}>
                  <ButtonToolbar>
                    <Button
                      appearance="primary"
                      block
                      onClick={e => this.goToChat(e)}
                    >
                      Agendar horário
                    </Button>
                  </ButtonToolbar>
                </FlexboxGrid.Item>
                <FlexboxGrid.Item colspan={22}>
                  <p className="mentorAvailable">
                    <strong>Disponível:</strong> {"Próximos mês"}
                  </p>
                </FlexboxGrid.Item>
              </FlexboxGrid>
            </Col>
          </Row>
        </Panel>
        <Panel bordered className="panel-padding">
          <Row className="verticalCenter">
            <Col md={5}>
              <FlexboxGrid justify="center">
                <FlexboxGrid.Item colspan={22} style={{ textAlign: "center" }}>
                  <AvatarProfile
                    name="Quero Educação"
                    size="150"
                    round={true}
                  />
                </FlexboxGrid.Item>
                <FlexboxGrid.Item
                  colspan={22}
                  style={{ textAlign: "center", marginTop: "1em" }}
                >
                  <Rating total={5} rating={2} interactive={true} />
                </FlexboxGrid.Item>
                <FlexboxGrid.Item
                  colspan={22}
                  style={{ textAlign: "center", marginTop: "0.4em" }}
                >
                  <span
                    style={{
                      fontSize: "12px"
                    }}
                  >
                    18 Avaliações
                  </span>
                </FlexboxGrid.Item>
              </FlexboxGrid>
            </Col>
            <Col md={13}>
              <div className="summary">
                <h2>Quero Educação</h2>
                <div className="degree">
                  <p>
                    <strong>Especialização:</strong> CELTA(Cambridge Certificate
                    in Teaching English) <strong>(Cambridge University)</strong>
                  </p>
                  <strong>São Paulo / SP </strong>
                </div>
              </div>

              <TagGroup className="tagGroup">
                {tag &&
                  tag.data &&
                  tag.data.map(item => (
                    <TagElement key={item.idTag} color="violet">
                      <span>{item.tag}</span>
                    </TagElement>
                  ))}
              </TagGroup>

              <div className="about">
                <p>
                  Professor 5 estrelas no Profes.15 anos de experiência.
                  Certificado CELTA Cambridge. Aulas personalizadas.Todos os
                  níveis. TOEFL,IELTS,TOEIC,SAT.
                </p>
              </div>
            </Col>
            <Col md={6}>
              <FlexboxGrid justify="space-around" align="middle">
                <FlexboxGrid.Item colspan={22}>
                  <ButtonToolbar>
                    <Button
                      appearance="primary"
                      block
                      onClick={e => this.goToChat(e)}
                    >
                      Agendar horário
                    </Button>
                  </ButtonToolbar>
                </FlexboxGrid.Item>
                <FlexboxGrid.Item colspan={22}>
                  <p className="mentorAvailable">
                    <strong>Disponível:</strong> {"Julho/2019"}
                  </p>
                </FlexboxGrid.Item>
              </FlexboxGrid>
            </Col>
          </Row>
        </Panel>
      </Fragment>
    );
  }
}

export default ProfileSummary;
