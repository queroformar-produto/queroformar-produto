import { withRouter } from "react-router-dom";

import MentorList from "./MentorList";

export default withRouter(MentorList);
