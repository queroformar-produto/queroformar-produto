import React, { Component, Fragment } from "react";
import { ProfileSummaryContainer } from "../../ProfileSummary/components";

import { Container, Header, Content, Panel, FlexboxGrid, Grid } from "rsuite";

import "./index.css";

export class MentorList extends Component {
  render() {
    return (
      <Fragment>
        <Container>
          <Header>
            <FlexboxGrid justify="center">
              <FlexboxGrid.Item colspan={6} style={{ textAlign: "center" }}>
                <h2>Mentoria na Faixa</h2>
                <p>No Quero Formar você encontra 42.385 mentores</p>
              </FlexboxGrid.Item>
            </FlexboxGrid>
          </Header>
          <Content className="content">
            <Grid fluid>
              <ProfileSummaryContainer />
            </Grid>
          </Content>
        </Container>
      </Fragment>
    );
  }
}

export default MentorList;
