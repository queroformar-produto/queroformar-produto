import React, { Component, Fragment } from "react";
import {
  Form,
  FormGroup,
  FormControl,
  ControlLabel,
  ButtonToolbar,
  Button,
  Container,
  Header,
  Content,
  Footer,
  Panel,
  FlexboxGrid
} from "rsuite";

export default class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: "",
      password: ""
    };

    this.handleInputEmailChange = this.handleInputEmailChange.bind(this);
    this.handleInputPasswordChange = this.handleInputPasswordChange.bind(this);
  }

  componentDidMount() {
    const { user, currentUrl } = this.props;

    if (user.access_token) {
      // Se usuário estiver autenticado, não permite que consiga acessar a URL de Login
      //currentUrl.goBack();
    }
  }

  handleInputEmailChange(value) {
    this.setState({
      email: value
    });
  }

  handleInputPasswordChange(value) {
    this.setState({
      password: value
    });
  }

  onClick = e => {
    e.preventDefault();

    let credentials = {
      email: this.state.email,
      password: this.state.password
    };

    const { login } = this.props;

    login(credentials);
  };

  render() {
    return (
      <Fragment>
        <Container style={{ background: "#4b1992", height: "100vh" }}>
          <Header>
            <FlexboxGrid justify="center">
              <FlexboxGrid.Item colspan={6} style={{ textAlign: "center" }}>
                <img
                  src="/Logo.svg"
                  alt="Logo Quero Formar"
                  width="250"
                  height="250"
                />
              </FlexboxGrid.Item>
            </FlexboxGrid>
          </Header>
          <Content>
            <FlexboxGrid justify="center">
              <FlexboxGrid.Item colspan={12} style={{ background: "#fbfbfb" }}>
                <Panel header={<h3>Entrar</h3>} bordered>
                  <Form fluid>
                    <FormGroup>
                      <ControlLabel>E-mail</ControlLabel>
                      <FormControl
                        name="email"
                        type="email"
                        value={this.state.email}
                        onChange={this.handleInputEmailChange}
                        placeholder={"E-mail"}
                      />
                    </FormGroup>
                    <FormGroup>
                      <ControlLabel>Senha</ControlLabel>
                      <FormControl
                        name="password"
                        type="password"
                        value={this.state.password}
                        onChange={this.handleInputPasswordChange}
                        placeholder={"Senha"}
                      />
                    </FormGroup>
                    <FormGroup>
                      <ButtonToolbar>
                        <Button appearance="primary" onClick={this.onClick}>
                          Entrar
                        </Button>
                      </ButtonToolbar>
                    </FormGroup>
                  </Form>
                </Panel>
              </FlexboxGrid.Item>
            </FlexboxGrid>
          </Content>
          <Footer
            style={{
              textAlign: "center",
              color: "#f2f2f2",
              marginBottom: "1em"
            }}
          >
            <p>Quero Formar 2019 - Todos os direitos reservados</p>
          </Footer>
        </Container>
      </Fragment>
    );
  }
}
