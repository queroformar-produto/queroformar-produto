import { compose } from "redux";
import { connect } from "react-redux";
import {
  authenticationLoggingIn,
  authenticationLoggedOut
} from "../__redux__/actions";

import Login from "./Login";

export default compose(
  connect(
    (state, ownProps) => ({
      user: state.user,
      currentUrl: ownProps.history
    }),
    {
      authenticationLoggingIn,
      authenticationLoggedOut
    },
    (stateProps, dispatchProps, parentProps) => ({
      ...stateProps,
      ...parentProps,
      login: credentials => dispatchProps.authenticationLoggingIn(credentials),
      logout: () => dispatchProps.authenticationLoggedOut()
    })
  )(Login)
);
