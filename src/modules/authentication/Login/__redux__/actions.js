import { HOST_API } from "../../../../constants/hosts";

import axios from "axios";

export const USER_LOGGING_IN = "USER_LOGGING_IN";
export const USER_LOGGED_IN = "USER_LOGGED_IN";
export const USER_LOGGED_OUT = "USER_LOGGED_OUT";

export const userLoggingIn = () => ({
  type: USER_LOGGING_IN
});

export const userLoggedIn = data => ({
  type: USER_LOGGED_IN,
  payload: data
});

export const userLoggedOut = () => ({
  type: USER_LOGGED_OUT
});

const shouldFetchUser = state => {
  const isAuthenticated = state.user ? state.user.access_token : false;

  if (!isAuthenticated) {
    return true;
  }

  return false;
};

export const authenticationLoggingIn = credentials => (dispatch, getState) => {
  if (shouldFetchUser(getState())) {
    dispatch(userLoggingIn());

    const { username, password } = credentials;

    axios
      .post(`${HOST_API}/users/oauth/token`, null, {
        params: {
          username,
          password
        }
      })
      .then(function(response) {
        dispatch(userLoggedIn(response.data));

        return axios.get(`${HOST_API}/users/me`).then(json => {
          const obj = Object.assign(response.data, json.data);

          // salva o state do user
          dispatch(userLoggedIn(obj));
        });
      })
      .catch(function(error) {});
  } else {
    return Promise.resolve();
  }
};

export const authenticationLoggedOut = () => dispatch => {
  return axios
    .post(`${HOST_API}/users/oauth/logout`, null, {})
    .then(function(response) {
      return dispatch(userLoggedOut());
    })
    .catch(function(error) {});
};
