import { USER_LOGGING_IN, USER_LOGGED_IN, USER_LOGGED_OUT } from "./actions";
import { SIGING_IN, SIGED_IN } from "../../Signup/__redux__/actions";

const initialState = {
  isLoading: false
};

export const user = (state = initialState, action) => {
  switch (action.type) {
    case USER_LOGGING_IN:
      return {
        ...state,
        isLoading: true
      };
    case USER_LOGGED_IN:
      return {
        ...state,
        isLoading: false
      };
    case USER_LOGGED_OUT:
      return {
        initialState
      };
    case SIGING_IN:
      return {
        ...state,
        isLoading: true
      };
    case SIGED_IN:
      return {
        ...state,
        isLoading: false
      };
    default:
      return state;
  }
};
