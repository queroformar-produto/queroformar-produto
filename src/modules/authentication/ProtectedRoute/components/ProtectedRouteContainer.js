// import { withRouter } from "react-router-dom";

// import ProtectedRoute from "./ProtectedRoute";

// export default withRouter(ProtectedRoute);

import { compose } from "redux";
import { connect } from "react-redux";

import ProtectedRoute from "./ProtectedRoute";

import { authenticationLoggedOut } from "../../Login/__redux__/actions";

export default compose(
  connect(
    (state, ownProps) => ({
      isLoggedIn: state.user.access_token,
      user: state.user,
      currentURL: ownProps.location ? ownProps.location.pathname : null
    }),
    {
      authenticationLoggedOut
    },
    (stateProps, dispatchProps, parentProps) => ({
      ...stateProps,
      ...parentProps,
      logout: () => dispatchProps.authenticationLoggedOut()
    })
  )(ProtectedRoute)
);
