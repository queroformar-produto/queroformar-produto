import React, { PureComponent, Fragment } from "react";
import NavBar from "../../../../UI/NavBar";

export default class Protected extends PureComponent {
  componentDidMount() {}

  render() {
    const { logout, isLoggedIn, user, publicationsList, t } = this.props;

    // if (isLoggedIn) {
    return (
      <Fragment>
        <NavBar />
        {this.props.children}
      </Fragment>
    );
    // } else {
    //   return null;
    // }
  }
}
