import { HOST_API } from "../../../../constants/hosts";

import axios from "axios";

export const SIGN_IN = "SIGN_IN";
export const SIGNED_IN = "SIGNED_IN";

export const siginIn = () => ({
  type: SIGN_IN
});

export const signedIn = data => ({
  type: SIGNED_IN,
  payload: data
});

export const signupAction = (formData, type) => (dispatch, getState) => {
  dispatch(siginIn());

  const { nome, cpf, email, senha } = formData;
  axios
  .post(`${HOST_API}/usuario/cadastrar?func=${type}`, {
    nome,
    cpf,
    senha,
    email
  }, {
    headers: {
      'Content-Type': 'application/json',
    }
  })
  .then(function(response) {
    dispatch(signedIn(response.data));
  })
  .catch(function(error) {});
};
