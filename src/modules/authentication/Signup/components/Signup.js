import React, { Component } from "react";
import { Form, FormGroup, Alert, Icon, Button, ButtonToolbar, FormControl, ControlLabel, JSONView, CustomField, RadioGroup, formError, Radio } from 'rsuite';
import "./index.css";
export class Signup extends Component {
  constructor(props) {
    super(props);

    this.state = {
      cpf: '',
      nome: '',
      senha: '',
      email: '',
      type: 0
    };

    this.handleInputEmailChange = this.handleInputEmailChange.bind(this);
    this.handleInputSenhaChange = this.handleInputSenhaChange.bind(this);
    this.handleInputCPFChange = this.handleInputCPFChange.bind(this);
    this.handleInputNomeChange = this.handleInputNomeChange.bind(this);

  }

  setType = (number) => {
    this.setState({
      type: number
    });
  }

  handleInputEmailChange(value) {
    this.setState({
      email: value
    });
  }

  handleInputSenhaChange(value) {
    this.setState({
      senha: value
    });
  }

  handleInputNomeChange(value) {
    this.setState({
      nome: value
    });
  }

  handleInputCPFChange(value) {
    this.setState({
      cpf: value
    });
  }

  signup = (e) => {
    e.preventDefault();

    const { signup } = this.props;
    const { nome, email, cpf, senha, type } = this.state;

    let formValue = {
      cpf,
      nome,
      senha,
      email
    }

    signup(formValue, type);
    this.props.history.push('/');
  }

  render() {
    const {type, nome, email, cpf, senha} = this.state;

    return (
      <div id="signup" className="container">
        <header>
          <img src="/Logo.svg" alt="Logo Quero Formar"/>
        </header>

        <h1>Crie sua conta gratuitamente</h1>

        <Form>
          <div className="user-type-area">
            <div className={`${type == 2 ? "selected" : ""}`} onClick={() => this.setType(2)}>
              <span>Quero ajuda!</span>
              <Icon icon='avatar' />
            </div>
            <div className={`${type == 1 ? "selected" : ""}`} onClick={() => this.setType(1)}>
              <span>Quero ajudar!</span>
              <Icon icon='people-group' />
            </div>
          </div>
          <FormGroup>
            <ControlLabel>Nome</ControlLabel>
            <FormControl value={nome} onChange={this.handleInputNomeChange} name="name" />
          </FormGroup>
          <FormGroup>
            <ControlLabel>Email</ControlLabel>
            <FormControl value={email} onChange={this.handleInputEmailChange} name="email" type="email" />
          </FormGroup>
          <FormGroup>
            <ControlLabel>Senha</ControlLabel>
            <FormControl value={senha} onChange={this.handleInputSenhaChange} name="password" type="password" />
          </FormGroup>
          <FormGroup>
            <ControlLabel>Confirme sua senha</ControlLabel>
            <FormControl name="password-confirm" type="password" />
          </FormGroup>
          <FormGroup>
            <ControlLabel>C.P.F</ControlLabel>
            <FormControl value={cpf} onChange={this.handleInputCPFChange} name="cpf" />
          </FormGroup>
          <FormGroup className="center">
            <ButtonToolbar>
              <Button appearance="primary" onClick={(e) => this.signup(e)}>Começar !</Button>
            </ButtonToolbar>
          </FormGroup>
        </Form>
      </div>
    );
  }
}

export default Signup;
