import { compose } from "redux";
import { connect } from "react-redux";
import { signupAction } from "../__redux__/actions";

import Signup from "./Signup";

export default compose(connect(
  (state, ownProps) => ({
    user: state.user,
    currentUrl: ownProps.history
  }),
  {
    signupAction
  },
  (stateProps, dispatchProps, parentProps) => ({
    ...stateProps,
    ...parentProps,
    signup: (formData, type) => dispatchProps.signupAction(formData, type)
  })
)(Signup));
