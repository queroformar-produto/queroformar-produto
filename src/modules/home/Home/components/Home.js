import React, { Component } from "react";
import "./index.css";
import { Button, Drawer, Form, FormGroup, ButtonToolbar, ControlLabel } from "rsuite";
import { UniversityListContainer } from "../../../university/UniversityList/components";
import { CourseListContainer } from "../../../course/CourseListByUniversity/components";
import AvatarProfile from "../../../../UI/AvatarProfile";

import { Sidenav } from 'rsuite';

import {
  BarChart, Bar, Cell, XAxis, YAxis, CartesianGrid, Tooltip, Legend, LineChart, Line,
} from 'recharts';

export class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      cnpjUniversidade: '',
      idCurso: '',
      placement: 'bottom',
      show: true,
      data: [
        {
          disciplina: 'Algoritmo', nota: 9,
        },
        {
          disciplina: 'L. P.', nota: 8,
        },
        {
          disciplina: 'Introd. BD', nota: 7,
        },
        {
          disciplina: 'P. O. Obj.', nota: 4,
        }
      ],
      periodoMedia: [
        {
          media: 6, periodo: '1º semestre',
        },
        {
          media: 8, periodo: '2º semestre',
        },
        {
          media: 7, periodo: '3º semestre',
        },
        {
          media: 8, periodo: '4º semestre',
        }
      ],
    };

    this.handleInputCNPJUniversidadeChange = this.handleInputCNPJUniversidadeChange.bind(this);
    this.handleInputIdCursoChange = this.handleInputIdCursoChange.bind(this);
    this.close = this.close.bind(this);
    this.toggleDrawer = this.toggleDrawer.bind(this);

  }

  componentDidMount() {
    const { getDiciplinas } = this.props;
    // getDiciplinas();
  }

  handleInputCNPJUniversidadeChange(value) {
    this.setState({
      cnpjUniversidade: value
    });
  }

  handleInputIdCursoChange(value) {
    this.setState({
      idCurso: value
    });
  }

  close() {
    const { selectedUniversity, selectedCourse, updateUser, user } = this.props;
    this.setState({
      show: false
    });
    const formData = {
      cnpjUniversidade: selectedUniversity ,
      idCurso: selectedCourse
    }
    updateUser(formData, user);
  }

  toggleDrawer(placement) {
    this.setState({
      placement,
      show: true
    });
  }

  render() {
    const { cnpjUniversidade, idCurso, placement, show, data, periodoMedia} = this.state;
    const { selectedCourseByUniversity, selectedUniversity } = this.props;

    return (
      <div className="home">
        <div class="right">
          <AvatarProfile
            name="Mentorado"
            size="170"
            round={true}
            src={"/images/user.jpeg"}
          />
          <ul className="user-details">
            <li> Unip </li>
            <li> Análise e Des. de Sistem. </li>
          </ul>
          <Form>
            <FormGroup className="center">
              <ButtonToolbar>
                <Button appearance="primary">Editar Perfil</Button>
              </ButtonToolbar>
            </FormGroup>
          </Form>
        </div>

        <h1>Acompanhamento {selectedCourseByUniversity.data}</h1>

        <div className="row row-1">
          <div className="graf-number graf-number-1 purple">
            <span>Tx. Sucesso</span>
            <span>90%</span>
          </div>
          <div className="graf-number purple">
            <span>Média Geral</span>
            <span>8</span>
          </div>
          <div className="graf-number purple">
            <span>Maior Nota</span>
            <span>10</span>
          </div>
        </div>

        <div className="row">
          <div className="graf">
            <h2>Desempenho por disciplina em aberto</h2>

            <BarChart
            width={500}
            height={300}
            data={data}
            margin={{
              top: 5, right: 0, left: 0, bottom: 5,
            }}
            >
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="disciplina" />
            <YAxis />
            <Tooltip />
            <Legend />
            <Bar dataKey="nota" fill="#8cc63f" />
            </BarChart>
          </div>

          <div className="graf">
            <h2>Média por período</h2>

            <LineChart
            width={500}
            height={300}
            data={periodoMedia}
            margin={{
              top: 5, right: 0, left: 0, bottom: 5,
            }}
            >
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="periodo" />
            <YAxis />
            <Tooltip />
            <Line connectNulls type="monotone" dataKey="media" stroke="#8884d8" fill="#8884d8" />
            </LineChart>
          </div>
        </div>



        {selectedCourseByUniversity && !selectedCourseByUniversity.data && selectedUniversity && !selectedUniversity.data && (
          <Drawer full placement={placement} show={show} onHide={this.close} className="myDrawer">
            <Drawer.Header>
              <Drawer.Title>Nos conte mais sobre você</Drawer.Title>
            </Drawer.Header>
            <Drawer.Body>

              <Form>
                <FormGroup>
                 <ControlLabel>Qual sua faculdade?</ControlLabel>
                 <UniversityListContainer/>
                </FormGroup>
                <FormGroup>
                  <ControlLabel>Seu curso?</ControlLabel>
                  <CourseListContainer/>
                </FormGroup>
              </Form>

            </Drawer.Body>
            <Drawer.Footer>
              <Button onClick={this.close} appearance="primary">
                Confirmar
              </Button>
            </Drawer.Footer>
          </Drawer>
        )}

      </div>
    );
  }
}

export default Home;
