import { compose } from "redux";
import { connect } from "react-redux";
import { updateUser, getDiciplinas } from "../__redux__/actions";

import Home from "./Home";

export default compose(connect(
  (state, ownProps) => ({
    user: state.user,
    selectedCourseByUniversity: state.selectedCourseByUniversity,
    selectedUniversity: state.selectedUniversity,
    currentUrl: ownProps.history,
  }),
  {
    updateUser
  },
  (stateProps, dispatchProps, parentProps) => ({
    ...stateProps,
    ...parentProps,
    updateUser: (formData, user) => dispatchProps.updateUser(formData, user),
    // getDiciplinas: () => dispatchProps.getDiciplinas(),
  })
)(Home));
