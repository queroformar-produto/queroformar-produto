import { HOST_API } from "../../../../constants/hosts";

import axios from "axios";

export const USER_UPDATE_IN = "USER_UPDATE_IN";
export const USER_UPDATED_IN = "USER_UPDATED_IN";
export const GET_DICIPLINAS_IN = "GET_DICIPLINAS_IN";
export const SET_DICIPLINAS_IN = "SET_DICIPLINAS_IN";

export const updateIn = () => ({
  type: USER_UPDATE_IN
});

export const updatedIn = data => ({
  type: USER_UPDATED_IN,
  payload: data
});

export const getDiciplinasIn = () => ({
  type: GET_DICIPLINAS_IN
});

export const setDiciplinasIn = (data) => ({
  type: SET_DICIPLINAS_IN,
  payload: data
});

export const updateUser = (formData, user) => (dispatch, getState) => {
  dispatch(updateIn());

  const { cnpjUniversidade, idCurso } = formData;
  const { cpf } = user;
  axios
  .post(`${HOST_API}/usuario/universidadeCurso?cpf=${cpf}`, {
    cnpjUniversidade,
    idCurso
  }, {
    headers: {
      'Content-Type': 'application/json',
    }
  })
  .then(function(response) {
    dispatch(updatedIn(response.data));
  })
  .catch(function(error) {});
};

export const getDiciplinas = () => (dispatch, getState) => {
  debugger
  dispatch(getDiciplinasIn());
  axios
  .post(`${HOST_API}/disciplina/disciplinaUsuario?cpf=cpf0`, {
  }, {
    headers: {
      'Content-Type': 'application/json',
    }
  })
  .then(function(response) {
    dispatch(setDiciplinasIn(response.data));
  })
  .catch(function(error) {});
};
