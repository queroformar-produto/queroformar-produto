import { GET_DICIPLINAS_IN, SET_DICIPLINAS_IN } from "./actions";

const initialState = {
  isLoading: false
};

export const home = (state = initialState, action) => {
  switch (action.type) {
    case GET_DICIPLINAS_IN:
      return {
        ...state,
        data: action.payload,
        isLoading: false
      };
    case SET_DICIPLINAS_IN:
      return {
        ...state,
        data: action.payload,
        isLoading: false
      };
    default:
      return state;
  }
};
