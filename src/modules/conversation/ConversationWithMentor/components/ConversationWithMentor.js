import React, { Component, Fragment } from "react";

import {
  Container,
  Header,
  Content,
  Button,
  FlexboxGrid,
  Drawer,
  Divider
} from "rsuite";

import ConversationThread from "../../../../UI/ConversationThread";

export class ConversationWithMentor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      show: false
    };
    this.close = this.close.bind(this);
    this.toggleDrawer = this.toggleDrawer.bind(this);
  }

  close() {
    this.setState({
      show: false
    });
  }
  toggleDrawer(placement) {
    this.setState({
      placement,
      show: true
    });
  }

  render() {
    const { placement, show } = this.state;

    return (
      <Fragment>
        <Container>
          <Header>
            <FlexboxGrid justify="center">
              <FlexboxGrid.Item colspan={6} style={{ textAlign: "center" }}>
                <h2>Mentoria na Faixa</h2>
                <p>No Quero Formar você encontra 42.385 mentores</p>
                <Divider />
              </FlexboxGrid.Item>
            </FlexboxGrid>
          </Header>
          <Content className="content">
            <ConversationThread />
            <Drawer full placement={placement} show={show} onHide={this.close}>
              <Drawer.Header>
                <Drawer.Title>Drawer Title</Drawer.Title>
              </Drawer.Header>
              <Drawer.Body />
              <Drawer.Footer>
                <Button onClick={this.close} appearance="primary">
                  Confirm
                </Button>
                <Button onClick={this.close} appearance="subtle">
                  Cancel
                </Button>
              </Drawer.Footer>
            </Drawer>
            ;
          </Content>
        </Container>
      </Fragment>
    );
  }
}

export default ConversationWithMentor;
