import { withRouter } from "react-router-dom";

import ConversationWithMentor from "./ConversationWithMentor";

export default withRouter(ConversationWithMentor);
